object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = #25968#25454#24211#21319#32423#27979#35797#31383#20307
  ClientHeight = 506
  ClientWidth = 763
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 64
    Top = 32
    Width = 48
    Height = 15
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 64
    Top = 56
    Width = 48
    Height = 15
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 208
    Top = 107
    Width = 120
    Height = 15
    Caption = #27979#35797#20869#23481#36755#20986#31383#20307
  end
  object Memo1: TMemo
    Left = 208
    Top = 128
    Width = 449
    Height = 345
    TabOrder = 0
  end
  object TestConnBtn: TButton
    Left = 56
    Top = 200
    Width = 75
    Height = 25
    Caption = #27979#35797#36830#25509
    TabOrder = 1
    OnClick = TestConnBtnClick
  end
  object UpgradeBtn: TButton
    Left = 56
    Top = 248
    Width = 75
    Height = 25
    Caption = #21319'  '#32423
    TabOrder = 2
    OnClick = UpgradeBtnClick
  end
  object UpgradeConn: TUniConnection
    LoginPrompt = False
    Left = 56
    Top = 112
  end
  object QueryTemp: TUniQuery
    Connection = UpgradeConn
    Left = 144
    Top = 112
  end
end
